(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     37108,       1014]
NotebookOptionsPosition[     36052,        975]
NotebookOutlinePosition[     36717,       1003]
CellTagsIndexPosition[     36608,        997]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"<<", "LiteRed`"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"<<", "Mint`"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"SetDim", "[", "d", "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"Declare", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"l", ",", "r", ",", "p", ",", "q"}], "}"}], ",", "Vector", ",", 
    "s", ",", "Number"}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"q", ",", "q"}], "]"}], "=", "0"}], ";", 
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"p", ",", "p"}], "]"}], "=", "0"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"q", ",", "p"}], "]"}], "=", 
   RowBox[{"s", "/", "2"}]}], ";"}]}], "Input",
 InitializationCell->True,
 CellChangeTimes->{{3.5907192949333944`*^9, 3.590719305507439*^9}, {
  3.590727156622458*^9, 3.590727196176838*^9}, {3.590727969182767*^9, 
  3.5907279699732943`*^9}, {3.5909064048979063`*^9, 3.590906406706115*^9}}],

Cell[BoxData[
 RowBox[{
  GraphicsBox[{
    {AbsoluteThickness[3], 
     LineBox[{{0.13375130616509928`, 0.8385579937304076}, {
      1.1452455590386625`, 0.5000000000000001}}]}, 
    {AbsoluteThickness[3], 
     LineBox[{{1.1452455590386625`, 0.5}, {0.13375130616509923`, 
      0.1405433646812959}}]}, 
    {AbsoluteThickness[3], 
     LineBox[{{0.39289446185997906`, 0.7507836990595612}, {0.6666666666666666,
       0.3286311389759665}}]}, 
    {AbsoluteThickness[3], 
     LineBox[{{0.39289446185997906`, 0.23667711598746077`}, {
      0.6666666666666666, 0.6630094043887147}}]}, 
    InsetBox["", {0.426332288401254, 0.39550679205851624`}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.4305120167189133, 0.38714733542319757`}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["2",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.438871473354232, 0.3787878787878789}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["3",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.438871473354232, 0.5877742946708471}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.522466039707419, 0.236677115987461}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5266457680250785, 0.22831765935214254`}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5350052246603971, 0.2199582027168241}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5182863113897598, 0.21159874608150564`}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["4",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.5266457680250785, 0.2032392894461872}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["5",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.522466039707419, 0.7507836990595613}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["6",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.8568443051201673, 0.3369905956112855}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.8819226750261233, 0.6379310344827589}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.865203761755486, 0.6379310344827589}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["7",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.8693834900731452, 0.6212121212121213}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["p",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.1839080459770115, 0.8594566353187045}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.13375130616509928`, 0.10292580982236177`}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["q",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[
       1.]], {0.13793103448275862`, 0.19905956112852685`}, {Left, Baseline},
     Alignment->{Left, Top}], 
    {AbsoluteThickness[3], 
     LineBox[{{1.1452455590386625`, 0.5000000000000002}, {1.2789968652037618`,
       0.5000000000000007}}]}, 
    {AbsoluteThickness[3], EdgeForm[{GrayLevel[0.], Opacity[1.], 
     AbsoluteThickness[3]}], 
     ArrowBox[{{0.13375130616509928`, 0.8385579937304077}, {
      0.3928944618599791, 0.7507836990595618}}]}, 
    {AbsoluteThickness[3], EdgeForm[{GrayLevel[0.], Opacity[1.], 
     AbsoluteThickness[3]}], 
     ArrowBox[{{0.13793103448275862`, 0.14054336468129613`}, {
      0.3894113554130181, 0.23180076579910391`}}]}, InsetBox[
     StyleBox[Cell["1:irreducible numerator",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.8693834900731452, 0.0778474399164053}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    StyleBox[InsetBox[
      StyleBox[Cell["v1",
        GeneratedCell->False,
        CellAutoOverwrite->False,
        CellBaseline->Baseline,
        TextAlignment->Left],
       Background->GrayLevel[
        1.]], {1.0574712643678161`, 0.8510971786833856}, {
      Left, Baseline}, {0.10449320794148381`, 0.09613375130616511}, {{1., 
      0.}, {0., 1.}},
      Alignment->{Left, Top}],
     FontSize->26]},
   ContentSelectable->True,
   ImagePadding->{{0., 0.}, {0., 0.}},
   ImageSize->{319., 240.},
   PlotRange->{{0., 1.3333333333333335`}, {0., 1.}},
   PlotRangePadding->Automatic], 
  GraphicsBox[{
    {AbsoluteThickness[3], 
     LineBox[{{0.13375130616509928`, 0.8385579937304076}, {
      1.1452455590386625`, 0.5000000000000001}}]}, 
    {AbsoluteThickness[3], 
     LineBox[{{1.1452455590386625`, 0.5}, {0.13375130616509923`, 
      0.1405433646812959}}]}, 
    InsetBox["", {0.426332288401254, 0.39550679205851624`}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.4305120167189133, 0.38714733542319757`}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["2",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.3301985370950888, 0.4832810867293631}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["3",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[
       1.]], {0.6060606060606062, 0.48328108672936354`}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.522466039707419, 0.236677115987461}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5266457680250785, 0.22831765935214254`}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5350052246603971, 0.2199582027168241}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5182863113897598, 0.21159874608150564`}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["4",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.5266457680250785, 0.2032392894461872}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["5",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.522466039707419, 0.7507836990595613}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["6",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.8568443051201673, 0.3369905956112855}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.8819226750261233, 0.6379310344827589}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.865203761755486, 0.6379310344827589}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["7",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.8693834900731452, 0.6212121212121213}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    {AbsoluteThickness[3], 
     LineBox[{{0.392894461859979, 0.22831765935214277`}, {0.392894461859979, 
      0.7507836990595618}}]}, 
    {AbsoluteThickness[3], 
     LineBox[{{0.6666666666666667, 0.6546499477533965}, {0.6666666666666667, 
      0.3286311389759675}}]}, InsetBox[
     StyleBox[Cell["p",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.1713688610240336, 0.8719958202716825}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["q",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[
       1.]], {0.13375130616509945`, 0.2157784743991642}, {Left, Baseline},
     Alignment->{Left, Top}], 
    {AbsoluteThickness[3], 
     LineBox[{{1.1452455590386628`, 0.5000000000000007}, {1.2706374085684433`,
       0.5000000000000011}}]}, 
    {AbsoluteThickness[3], EdgeForm[{GrayLevel[0.], Opacity[1.], 
     AbsoluteThickness[3]}], 
     ArrowBox[{{0.13375130616509967`, 0.8385579937304077}, {
      0.39289446185997945`, 0.7507836990595622}}]}, 
    {AbsoluteThickness[3], EdgeForm[{GrayLevel[0.], Opacity[1.], 
     AbsoluteThickness[3]}], 
     ArrowBox[{{0.13375130616509967`, 0.14054336468129613`}, {
      0.39289446185997945`, 0.2366771159874621}}]}, InsetBox[
     StyleBox[Cell["1:irreducible numerator",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.8589341692789969, 0.08620689655172398}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    StyleBox[InsetBox[
      StyleBox[Cell["v2",
        GeneratedCell->False,
        CellAutoOverwrite->False,
        CellBaseline->Baseline,
        TextAlignment->Left],
       Background->GrayLevel[
        1.]], {1.0867293625914318`, 0.8155694879832807}, {
      Left, Baseline}, {0.10449320794148381`, 0.09613375130616511}, {{1., 
      0.}, {0., 1.}},
      Alignment->{Left, Top}],
     FontSize->26]},
   ContentSelectable->True,
   ImagePadding->{{0., 0.}, {0., 0.}},
   ImageSize->{319., 240.},
   PlotRange->{{0., 1.3333333333333335`}, {0., 1.}},
   PlotRangePadding->Automatic], 
  GraphicsBox[{
    {AbsoluteThickness[3], 
     LineBox[{{0.13375130616509928`, 0.8385579937304076}, {
      1.1452455590386625`, 0.5000000000000001}}]}, 
    {AbsoluteThickness[3], 
     LineBox[{{1.1452455590386625`, 0.5}, {0.13375130616509923`, 
      0.1405433646812959}}]}, 
    InsetBox["", {0.426332288401254, 0.39550679205851624`}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.4305120167189133, 0.38714733542319757`}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["2",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.3427377220480669, 0.345350052246604}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["3",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[
       1.]], {0.34273772204806696`, 0.5877742946708466}, {Left, Baseline}, 
     {0.025078369905956122`, 0.05015673981191223}, {{1., 0.}, {0., 1.}},
     Alignment->{Left, Top}], 
    InsetBox["", {0.522466039707419, 0.236677115987461}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5266457680250785, 0.22831765935214254`}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5350052246603971, 0.2199582027168241}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.5182863113897598, 0.21159874608150564`}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["4",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.5517241379310347, 0.4832810867293629}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["5",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[
       1.]], {0.7314524555903865, 0.28683385579937304`}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["6",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.5642633228840126, 0.7089864158829684}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.8819226750261233, 0.6379310344827589}, {Left, Baseline},
     Alignment->{Left, Top}], 
    InsetBox["", {0.865203761755486, 0.6379310344827589}, {Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["7",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.8693834900731452, 0.6212121212121213}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    {AbsoluteThickness[3], 
     LineBox[{{0.392894461859979, 0.22831765935214277`}, {0.392894461859979, 
      0.7507836990595618}}]}, InsetBox[
     StyleBox[Cell["p",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.1713688610240336, 0.8719958202716825}, {
     Left, Baseline},
     Alignment->{Left, Top}], InsetBox[
     StyleBox[Cell["q",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[
       1.]], {0.13375130616509945`, 0.2157784743991642}, {Left, Baseline},
     Alignment->{Left, Top}], 
    {AbsoluteThickness[3], 
     LineBox[{{1.1452455590386628`, 0.5000000000000007}, {1.2706374085684433`,
       0.5000000000000011}}]}, 
    {AbsoluteThickness[3], EdgeForm[{GrayLevel[0.], Opacity[1.], 
     AbsoluteThickness[3]}], 
     ArrowBox[{{0.13375130616509967`, 0.8385579937304077}, {
      0.39289446185997945`, 0.7507836990595622}}]}, 
    {AbsoluteThickness[3], EdgeForm[{GrayLevel[0.], Opacity[1.], 
     AbsoluteThickness[3]}], 
     ArrowBox[{{0.13375130616509967`, 0.14054336468129613`}, {
      0.39289446185997945`, 0.2366771159874621}}]}, InsetBox[
     StyleBox[Cell["1:irreducible numerator",
       GeneratedCell->False,
       CellAutoOverwrite->False,
       CellBaseline->Baseline,
       TextAlignment->Left],
      Background->GrayLevel[1.]], {0.8589341692789969, 0.08620689655172398}, {
     Left, Baseline},
     Alignment->{Left, Top}], 
    StyleBox[InsetBox[
      StyleBox[Cell["v3",
        GeneratedCell->False,
        CellAutoOverwrite->False,
        CellBaseline->Baseline,
        TextAlignment->Left],
       Background->GrayLevel[
        1.]], {1.0867293625914318`, 0.8155694879832807}, {
      Left, Baseline}, {0.10449320794148381`, 0.09613375130616511}, {{1., 
      0.}, {0., 1.}},
      Alignment->{Left, Top}],
     FontSize->26], 
    {AbsoluteThickness[3], 
     LineBox[{{0.3928944618599792, 0.47492163009404464`}, {0.756530825496343, 
      0.62957157784744}}]}},
   ContentSelectable->True,
   ImagePadding->{{0., 0.}, {0., 0.}},
   ImageSize->{319., 240.},
   PlotRange->{{0., 1.3333333333333335`}, {0., 1.}},
   PlotRangePadding->Automatic]}]], "Text",
 CellChangeTimes->{{3.5907326600589495`*^9, 3.5907330431252103`*^9}, 
   3.590733115857484*^9, {3.590816964196176*^9, 3.5908169652338657`*^9}, {
   3.5908854570186853`*^9, 3.5908854650060487`*^9}, {3.590901500698041*^9, 
   3.5909015284181714`*^9}, {3.5909015924502716`*^9, 3.5909016666080537`*^9}}],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{"EMs", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      SubscriptBox[
       StyleBox["rules", "TI"], "1"], ",", 
      SubscriptBox[
       StyleBox["rules", "TI"], "2"], ",", "\[Ellipsis]"}], "}"}]}], 
   TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " is a an option for ",
 StyleBox["FindSymmetries",
  FontWeight->"Bold"],
 " and ",
 StyleBox["FindExtSymmetries",
  FontWeight->"Bold"],
 ". Each  ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    StyleBox["rules", "TI"], "i"], TraditionalForm]]],
 " is a set of substitutions which preserves the external invariants. This \
option helps LiteRed to find symmetries between sectors which are connect to \
each other by substitutions of external momenta."
}], "Text",
 CellChangeTimes->{{3.5909065798980393`*^9, 3.590906594949145*^9}, {
  3.590906643165532*^9, 3.590906786765108*^9}, {3.590906825381033*^9, 
  3.590906941164767*^9}, {3.5909070411166286`*^9, 3.590907208387751*^9}},
 CellTags->"EMs option"],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{"FindExtSymmetries", "[", 
    RowBox[{
     SubscriptBox[
      StyleBox["basis", "TI"], "1"], ",", 
     SubscriptBox[
      StyleBox["basis", "TI"], "2"]}], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " finds the mappings of the sectors of ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    StyleBox["basis", "TI"], "1"], TraditionalForm]],
  FontWeight->"Bold"],
 " to unique sectors of ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    StyleBox["basis", "TI"], "2"], TraditionalForm]]],
 ". It generates the following objects:\n    ",
 Cell[BoxData[
  FormBox[
   RowBox[{"ExtUniqueSectors", "[", 
    SubscriptBox[
     StyleBox["basis", "TI"], "1"], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " \[LongDash] the list of sectors which can not be mapped,\n    ",
 Cell[BoxData[
  FormBox[
   RowBox[{"ExtMappedSectors", "[", 
    SubscriptBox[
     StyleBox["basis", "TI"], "1"], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " \[LongDash] the list of sectors, equivalent to some unique sectors of ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    StyleBox["basis", "TI"], "2"], TraditionalForm]]],
 ",\n    ",
 Cell[BoxData[
  FormBox[
   RowBox[{"jExtRules", "[", 
    RowBox[{
     SubscriptBox[
      StyleBox["basis", "TI"], "1"], ",", 
     RowBox[{"ns", ":", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"0", "|", "1"}], ")"}], ".."}]}]}], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " for each sector ",
 Cell[BoxData[
  FormBox[
   RowBox[{"js", "[", 
    RowBox[{
     SubscriptBox[
      StyleBox["basis", "TI"], "1"], ",", 
     RowBox[{"ns", ":", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"0", "|", "1"}], ")"}], ".."}]}]}], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " in ",
 Cell[BoxData[
  FormBox[
   RowBox[{"ExtMappedSectors", "[", 
    SubscriptBox[
     StyleBox["basis", "TI"], "1"], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " \[LongDash] the list of rules mapping integrals to some unique sector of ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    StyleBox["basis", "TI"], "2"], TraditionalForm]]],
 ".\nNote that ",
 Cell[BoxData[
  FormBox[
   RowBox[{"FindExtSymmetries", "[", 
    RowBox[{
     SubscriptBox[
      StyleBox["basis", "TI"], "1"], ",", 
     SubscriptBox[
      StyleBox["basis", "TI"], "2"]}], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " requires running ",
 Cell[BoxData[
  FormBox[
   RowBox[{"AnalyzeSectors", "[", 
    SubscriptBox[
     StyleBox["basis", "TI"], "1"], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " and ",
 Cell[BoxData[
  FormBox[
   RowBox[{"FindSymmetries", "[", 
    SubscriptBox[
     StyleBox["basis", "TI"], "2"], "]"}], TraditionalForm]],
  FontWeight->"Bold"],
 " beforehand. If afterwards ",
 Cell[BoxData[
  FormBox[
   RowBox[{"FindExtSymmetries", "[", 
    RowBox[{
     SubscriptBox[
      StyleBox["basis", "TI"], "1"], ",", 
     SubscriptBox[
      StyleBox["basis", "TI"], "3"]}], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " is called, it tries to map only those sectors which are in ",
 Cell[BoxData[
  FormBox[
   RowBox[{"ExtUniqueSectors", "[", 
    SubscriptBox[
     StyleBox["basis", "TI"], "1"], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " and modifies correspondingly the lists ",
 Cell[BoxData[
  FormBox[
   RowBox[{"ExtUniqueSectors", "[", 
    SubscriptBox[
     StyleBox["basis", "TI"], "1"], "]"}], TraditionalForm]],
  FontWeight->"Bold"],
 " and  ",
 Cell[BoxData[
  FormBox[
   RowBox[{"ExtMappedSectors", "[", 
    SubscriptBox[
     StyleBox["basis", "TI"], "1"], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 ".\n\n",
 Cell[BoxData[
  FormBox[
   RowBox[{"FindExtSymmetries", "[", 
    RowBox[{
     SubscriptBox[
      StyleBox["basis", "TI"], "1"], ",", 
     SubscriptBox[
      StyleBox["basis", "TI"], "2"], ",", "\[Ellipsis]", ",", 
     SubscriptBox[
      StyleBox["basis", "TI"], "k"]}], "]"}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " is a shortcut for ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"FindExtSymmetries", "[", 
     RowBox[{
      SubscriptBox[
       StyleBox["basis", "TI"], "1"], ",", 
      SubscriptBox[
       StyleBox["basis", "TI"], "k"]}], "]"}], ";", 
    RowBox[{"FindExtSymmetries", "[", 
     RowBox[{
      SubscriptBox[
       StyleBox["basis", "TI"], "1"], ",", 
      SubscriptBox[
       StyleBox["basis", "TI"], 
       RowBox[{"k", "-", "1"}]]}], "]"}], ";", 
    RowBox[{"\[Ellipsis]FindExtSymmetries", "[", 
     RowBox[{
      SubscriptBox[
       StyleBox["basis", "TI"], "1"], ",", 
      SubscriptBox[
       StyleBox["basis", "TI"], "2"]}], "]"}]}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontWeight->"Bold"],
 " \[LongDash] pay attention to the order!"
}], "Text",
 CellChangeTimes->{
  3.5909073893747625`*^9, {3.590907434202859*^9, 3.590907841704357*^9}, {
   3.590907913712708*^9, 3.590908001791366*^9}},
 CellTags->"FindExtSymmetries"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"NewBasis", "[", 
   RowBox[{"v1", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"r", "-", "l"}], ",", "l", ",", "r", ",", 
      RowBox[{"l", "+", "q"}], ",", 
      RowBox[{"r", "+", "p"}], ",", 
      RowBox[{"l", "+", "q", "-", "r"}], ",", 
      RowBox[{"r", "+", "p", "-", "l"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"l", ",", "r"}], "}"}], ",", 
    RowBox[{"GenerateIBP", "\[Rule]", "True"}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AnalyzeSectors", "[", 
   RowBox[{"v1", ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "__"}], "}"}]}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"FindSymmetries", "[", 
    RowBox[{"v1", ",", 
     RowBox[{"EMs", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"(*", 
          RowBox[{
           RowBox[{"p", "\[Rule]", "p"}], ",", 
           RowBox[{"q", "\[Rule]", "q"}]}], "*)"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"p", "\[Rule]", "q"}], ",", 
          RowBox[{"q", "\[Rule]", "p"}]}], "}"}]}], "}"}]}]}], "]"}], ";"}], 
  RowBox[{"(*", 
   ButtonBox[
    RowBox[{"See", " ", "EMs", " ", "option", " ", "descrition"}],
    BaseStyle->"Hyperlink",
    ButtonData->"EMs option"], "*)"}]}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.5907053038171854`*^9, 3.590705317166078*^9}, {
   3.5907053697441053`*^9, 3.590705424556626*^9}, {3.5907054988791428`*^9, 
   3.5907055612007165`*^9}, 3.5907272023929825`*^9, {3.590727854446274*^9, 
   3.5907278750179796`*^9}, {3.59072794295129*^9, 3.590728001699428*^9}, {
   3.590728097843536*^9, 3.59072811054506*^9}, {3.590729780271654*^9, 
   3.590729852804061*^9}, {3.590883956072588*^9, 3.590883959577942*^9}, 
   3.5908840211653433`*^9, {3.5908844268047533`*^9, 3.5908844605273886`*^9}, {
   3.590886763584309*^9, 3.590886789782898*^9}, 3.590887179545574*^9, 
   3.590888080643726*^9, 3.5908981753813906`*^9, {3.590906465136407*^9, 
   3.5909065420246124`*^9}, 3.5909065778246465`*^9, 3.5909072263938437`*^9, {
   3.590907263006603*^9, 3.5909073053752165`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"NewBasis", "[", 
   RowBox[{"v2", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"r", "-", "l"}], ",", "l", ",", "r", ",", 
      RowBox[{"l", "+", "q"}], ",", 
      RowBox[{"l", "-", "p"}], ",", 
      RowBox[{"l", "+", "q", "-", "r"}], ",", 
      RowBox[{"r", "+", "p", "-", "l"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"l", ",", "r"}], "}"}], ",", 
    RowBox[{"GenerateIBP", "\[Rule]", "True"}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AnalyzeSectors", "[", 
   RowBox[{"v2", ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "__"}], "}"}]}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"FindExtSymmetries", "[", 
    RowBox[{"v2", ",", "v1", ",", 
     RowBox[{"EMs", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"p", "\[Rule]", "q"}], ",", 
          RowBox[{"q", "\[Rule]", "p"}]}], "}"}]}], "}"}]}]}], "]"}], ";", 
   RowBox[{"(*", 
    ButtonBox[
     RowBox[{"See", " ", "FindExtSymmetries", " ", "description"}],
     BaseStyle->"Hyperlink",
     ButtonData->"FindExtSymmetries"], "*)"}], 
   RowBox[{"(*", 
    ButtonBox[
     RowBox[{"See", " ", "EMs", " ", "option", " ", "descrition"}],
     BaseStyle->"Hyperlink",
     ButtonData->"EMs option"], "*)"}], "\n", 
   RowBox[{"(*", 
    RowBox[{
     RowBox[{"This", " ", "is", " ", "new", " ", 
      RowBox[{"procedure", ".", " ", "Omitting"}], " ", "the", " ", "option", 
      " ", "EMs"}], "\[Rule]", 
     RowBox[{
      RowBox[{"...", " ", "is", " ", "equivalent", " ", "to", " ", "EMs"}], 
      "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"{", "}"}], "}"}]}]}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"FindSymmetries", "[", 
    RowBox[{"v2", ",", 
     RowBox[{"EMs", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"(*", 
          RowBox[{
           RowBox[{"p", "\[Rule]", "p"}], ",", 
           RowBox[{"q", "\[Rule]", "q"}]}], "*)"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"p", "\[Rule]", "q"}], ",", 
          RowBox[{"q", "\[Rule]", "p"}]}], "}"}]}], "}"}]}]}], "]"}], ";"}], 
  RowBox[{"(*", 
   ButtonBox[
    RowBox[{"See", " ", "EMs", " ", "option", " ", "descrition"}],
    BaseStyle->"Hyperlink",
    ButtonData->"EMs option"], "*)"}]}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.5907053038171854`*^9, 3.590705317166078*^9}, {
   3.5907053697441053`*^9, 3.590705424556626*^9}, {3.5907054988791428`*^9, 
   3.5907055612007165`*^9}, 3.5907272023929825`*^9, {3.590727854446274*^9, 
   3.5907278750179796`*^9}, {3.59072794295129*^9, 3.590728001699428*^9}, {
   3.590728097843536*^9, 3.59072811054506*^9}, {3.590729780271654*^9, 
   3.590729916472479*^9}, {3.59072995278767*^9, 3.5907299529617896`*^9}, 
   3.590730577734047*^9, {3.590731891727868*^9, 3.5907320144015975`*^9}, {
   3.5908846303684196`*^9, 3.590884646658353*^9}, 3.5908872117852163`*^9, 
   3.590888047141238*^9, 3.5908982005172663`*^9, 3.590906476621685*^9, {
   3.590907314057104*^9, 3.590907316854985*^9}, {3.5909083852450953`*^9, 
   3.590908416681201*^9}, 3.5909084584342327`*^9}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"NewBasis", "[", 
   RowBox[{"v3", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"r", "-", "q"}], ",", "l", ",", 
      RowBox[{"r", "-", "l"}], ",", "r", ",", 
      RowBox[{"l", "+", "q"}], ",", 
      RowBox[{"r", "+", "p", "-", "l"}], ",", 
      RowBox[{"l", "-", "p"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"l", ",", "r"}], "}"}], ",", 
    RowBox[{"GenerateIBP", "\[Rule]", "True"}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AnalyzeSectors", "[", 
   RowBox[{"v3", ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "__"}], "}"}]}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"FindExtSymmetries", "[", 
    RowBox[{"v3", ",", "v2", ",", "v1", ",", 
     RowBox[{"EMs", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"(*", 
          RowBox[{
           RowBox[{"p", "\[Rule]", "p"}], ",", 
           RowBox[{"q", "\[Rule]", "q"}]}], "*)"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"p", "\[Rule]", "q"}], ",", 
          RowBox[{"q", "\[Rule]", "p"}]}], "}"}]}], "}"}]}]}], "]"}], ";", 
   RowBox[{"(*", 
    ButtonBox[
     RowBox[{"See", " ", "FindExtSymmetries", " ", "description"}],
     BaseStyle->"Hyperlink",
     ButtonData->"FindExtSymmetries"], "*)"}], 
   RowBox[{"(*", 
    ButtonBox[
     RowBox[{"See", " ", "EMs", " ", "option", " ", "descrition"}],
     BaseStyle->"Hyperlink",
     ButtonData->"EMs option"], "*)"}], "\n", 
   RowBox[{"(*", 
    RowBox[{
     RowBox[{"This", " ", "is", " ", "new", " ", 
      RowBox[{"procedure", ".", " ", "Omitting"}], " ", "the", " ", "option", 
      " ", "EMs"}], "\[Rule]", 
     RowBox[{
      RowBox[{"...", " ", "is", " ", "equivalent", " ", "to", " ", "EMs"}], 
      "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"{", "}"}], "}"}]}]}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"FindSymmetries", "[", 
    RowBox[{"v3", ",", 
     RowBox[{"EMs", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"(*", 
          RowBox[{
           RowBox[{"p", "\[Rule]", "p"}], ",", 
           RowBox[{"q", "\[Rule]", "q"}]}], "*)"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"p", "\[Rule]", "q"}], ",", 
          RowBox[{"q", "\[Rule]", "p"}]}], "}"}]}], "}"}]}]}], "]"}], ";"}], 
  RowBox[{"(*", 
   ButtonBox[
    RowBox[{"See", " ", "EMs", " ", "option", " ", "descrition"}],
    BaseStyle->"Hyperlink",
    ButtonData->"EMs option"], "*)"}]}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.5907053038171854`*^9, 3.590705317166078*^9}, {
   3.5907053697441053`*^9, 3.590705424556626*^9}, {3.5907054988791428`*^9, 
   3.5907055612007165`*^9}, 3.5907272023929825`*^9, {3.590727854446274*^9, 
   3.5907278750179796`*^9}, {3.59072794295129*^9, 3.590728001699428*^9}, {
   3.590728097843536*^9, 3.59072811054506*^9}, {3.590729780271654*^9, 
   3.590729916472479*^9}, {3.59072995278767*^9, 3.5907299529617896`*^9}, 
   3.590730577734047*^9, {3.590731891727868*^9, 3.5907320144015975`*^9}, {
   3.5908846303684196`*^9, 3.590884646658353*^9}, 3.5908872117852163`*^9, {
   3.5908877812792788`*^9, 3.5908877868460164`*^9}, {3.5908878205346327`*^9, 
   3.590887820814822*^9}, {3.5908879634920435`*^9, 3.5908880150216722`*^9}, {
   3.590900454306037*^9, 3.590900465142312*^9}, {3.5909084452854047`*^9, 
   3.590908461362193*^9}}],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"SolvejSector", "[", 
     RowBox[{"UniqueSectors", "[", "v1", "]"}], "]"}], ",", 
    RowBox[{"SolvejSector", "[", 
     RowBox[{"UniqueSectors", "[", "v2", "]"}], "]"}], ",", 
    RowBox[{"SolvejSector", "[", 
     RowBox[{"UniqueSectors", "[", "v3", "]"}], "]"}]}], "}"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.5907053038171854`*^9, 3.590705317166078*^9}, {
   3.5907053697441053`*^9, 3.590705424556626*^9}, {3.5907054988791428`*^9, 
   3.5907055612007165`*^9}, 3.5907272023929825`*^9, {3.590727854446274*^9, 
   3.5907278750179796`*^9}, {3.59072794295129*^9, 3.590728001699428*^9}, {
   3.590728097843536*^9, 3.59072811054506*^9}, {3.590729780271654*^9, 
   3.590729852804061*^9}, {3.590883956072588*^9, 3.590883959577942*^9}, 
   3.5908840211653433`*^9, {3.5908844268047533`*^9, 3.5908844605273886`*^9}, {
   3.590886763584309*^9, 3.590886789782898*^9}, 3.590887179545574*^9, 
   3.590888080643726*^9, 3.5908981753813906`*^9, {3.590901572236701*^9, 
   3.5909015771960297`*^9}, {3.5909026717459497`*^9, 
   3.5909026790098248`*^9}, {3.5909088342262115`*^9, 3.5909088421525383`*^9}}],

Cell[TextData[{
 "SolvejSector\[CloseCurlyQuote]s option ",
 StyleBox["NMIs\[Rule]",
  FontWeight->"Bold"],
 StyleBox["n", "TI"],
 ", where ",
 StyleBox["n", "TI"],
 " is some integer, prescribes ",
 StyleBox["SolvejSector",
  FontWeight->"Bold"],
 " to stop when the number of integrals not yet reducible is n\n",
 StyleBox["NMIs\[Rule]Automatic",
  FontWeight->"Bold"],
 " uses Mint, if it is loaded, to determine ",
 StyleBox["n", "TI"],
 "."
}], "Text",
 CellChangeTimes->{{3.5909084877048836`*^9, 3.590908529647113*^9}, {
  3.590908578543941*^9, 3.59090879225403*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"SolvejSector", ",", 
    RowBox[{"NMIs", "\[Rule]", "Automatic"}]}], "]"}], ";"}], "\n", 
 RowBox[{"Timing", "[", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"SolvejSector", "[", 
     RowBox[{"UniqueSectors", "[", "v1", "]"}], "]"}], ",", 
    RowBox[{"SolvejSector", "[", 
     RowBox[{"UniqueSectors", "[", "v2", "]"}], "]"}], ",", 
    RowBox[{"SolvejSector", "[", 
     RowBox[{"UniqueSectors", "[", "v3", "]"}], "]"}]}], "}"}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.5909029817366686`*^9, 3.5909030026797295`*^9}, {
   3.5909064468250604`*^9, 3.590906449721006*^9}, 3.590908849011138*^9}],

Cell[BoxData[
 RowBox[{"IBPReduce", "[", 
  RowBox[{
   RowBox[{"j", "[", 
    RowBox[{"v2", ",", 
     RowBox[{"-", "2"}], ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", 
     ",", "1"}], "]"}], "+", 
   RowBox[{"j", "[", 
    RowBox[{"v1", ",", 
     RowBox[{"-", "2"}], ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", 
     ",", "1"}], "]"}], "+", 
   RowBox[{"j", "[", 
    RowBox[{"v3", ",", 
     RowBox[{"-", "2"}], ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", 
     ",", "1"}], "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.590719945286047*^9, 3.590719955688979*^9}, {
  3.590729459486889*^9, 3.590729490780736*^9}, {3.5907320593725595`*^9, 
  3.590732063206113*^9}, {3.5909088625212126`*^9, 3.590908865168991*^9}}]
},
WindowSize->{1560, 865},
WindowMargins->{{0, Automatic}, {1, Automatic}},
ShowSelection->True,
CellContext->CellGroup,
ShowCellTags->True,
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "EMs option"->{
  Cell[17723, 451, 1043, 31, 63, "Text",
   CellTags->"EMs option"]},
 "FindExtSymmetries"->{
  Cell[18769, 484, 5365, 185, 177, "Text",
   CellTags->"FindExtSymmetries"]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"EMs option", 36412, 988},
 {"FindExtSymmetries", 36506, 991}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 1084, 31, 152, "Input",
 InitializationCell->True],
Cell[1644, 53, 16076, 396, 256, "Text"],
Cell[17723, 451, 1043, 31, 63, "Text",
 CellTags->"EMs option"],
Cell[18769, 484, 5365, 185, 177, "Text",
 CellTags->"FindExtSymmetries"],
Cell[24137, 671, 2140, 51, 92, "Input"],
Cell[26280, 724, 3204, 81, 132, "Input"],
Cell[29487, 807, 3409, 86, 132, "Input"],
Cell[32899, 895, 1170, 21, 31, "Input"],
Cell[34072, 918, 573, 18, 49, "Text"],
Cell[34648, 938, 662, 16, 52, "Input"],
Cell[35313, 956, 735, 17, 31, "Input"]
}
]
*)

(* End of internal cache information *)
